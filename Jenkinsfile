pipeline {
    agent any
    tools {
        nodejs "node"
     }
    stages {
        stage('increment version') {
            when {
                expression {
                    return env.GIT_BRANCH == "main"  
                }
            }          
            steps {
                script {
                    // enter app directory, because that's where package.json is located
                    dir("app") {
                        // update application version in the package.json file with one of these release types: patch, minor or major
                        // this will commit the version update
                        sh "npm version minor"

                        // read the updated version from the package.json file
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version

                        // set the new version as part of IMAGE_NAME
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                    }

                    // alternative solution without Pipeline Utility Steps plugin: 
                    // def version = sh (returnStdout: true, script: "grep 'version' package.json | cut -d '\"' -f4 | tr '\\n' '\\0'")
                    // env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Run tests') {
            steps {
               script {
                    // enter app directory, because thats where package.json and tests are located
                    dir("app") {
                        // install all dependencies needed for running tests
                        sh "npm install"
                        sh "npm run test"
                    } 
               }
            }
        }
        stage('Build and Push docker image') {
            when {
                expression {
                    return env.GIT_BRANCH == "main"  
                }
            }          
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]){
                    sh "docker build -t n8xx/demo-app:${IMAGE_NAME} ."
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh "docker push n8xx/demo-app:${IMAGE_NAME}"
                }
            }
        }
        stage('deploy to EC2') {
            when {
                expression {
                    return env.GIT_BRANCH == "main"  
                }
            }          
            steps {
                script {
                def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                def ec2Instance = "ec2-user@35.88.29.30"

                sshagent(['ec2-server-key']) {
                //sh "ssh -o StrictHostKeyChecking=no ec2-user@35.88.29.30 echo 'Hello'"

                sh "scp -o StrictHostKeyChecking=no server-cmds.sh ec2-user@35.88.29.30:/home/ec2-user"
                sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                }
            }
        }
    }


        stage('commit version update') {
            when {
                expression {
                    return env.GIT_BRANCH == "main"  
                }
            }          
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        // git config here for the first time run
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/n8-project1/aws-exercises.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
